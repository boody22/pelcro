<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [AuthController::class, 'login']);
});

