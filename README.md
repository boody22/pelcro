
Since the task is a CRUD operation I preferred to keep it simple and put the logic in the controller and if we have a vision it will be scaled by using service and repository pattern

## Requirements

In order to run this project you just only have Docker and Docker-compose installed.

## Up & Running

Once you have cloned the project run:
> cp .env.example .env
>
> docker run --rm --interactive --tty   --volume $PWD:/app   --user $(id -u):$(id -g)   composer install

> ./vendor/bin/sail  up -d

> ./vendor/bin/sail artisan migrate



Register new user through bas_url/register.

You can get an access token the registered email & password

>POST http://localhost:8000/api/auth/login

create new Customer:

```json
{
    "first_name":"Ahmed",
    "last_name":"Omar",
    "email":"test@gmail.com",
    "user_name":"test22",
    "salary":100,
    "status":1
}
```
>POST http://base_url/api/customers

update an existing Customer:

```json
{
    "first_name":"Ahmed",
    "last_name":"Omar",
    "email":"test@gmail.com",
    "user_name":"test22",
    "salary":100,
    "status":1
}
```

>PATCH http://base_url/api/customers/{id}

list all customers:
>GET http://base_url/api/customers

Show customer:

>GET http://base_url/api/customers/{id}

Delete customer:

>delete http://base_url/api/customers/{id}

You Can also make the CRUD operations through the browser from this link
http://base_url/dasboard

