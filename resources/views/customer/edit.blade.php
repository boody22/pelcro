<x-app-layout>
    @if($errors->any())
        {{ implode('', $errors->all(':message')) }}
    @endif
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <form method="POST" action="{{ route('customers.update', ['customer'=>1]) }}" class="mt-6 space-y-6">
                @csrf
                @method('patch')

                <!-- First Name -->
                <div>
                    <x-input-label for="name" :value="__('First Name')" />
                    <x-text-input id="name" class="block mt-1 w-full" type="text" name="first_name" value="{{$customer->first_name}}" required autofocus autocomplete="first_name" />
                    <x-input-error :messages="$errors->get('first_name')" class="mt-2" />
                </div>
                <!-- Last Name -->
                <div>
                    <x-input-label for="name" :value="__('Last Name')" />
                    <x-text-input id="name" class="block mt-1 w-full" type="text" name="last_name" value="{{$customer->last_name}}" required autofocus autocomplete="last_name" />
                    <x-input-error :messages="$errors->get('last_name')" class="mt-2" />
                </div>

                <!-- Email Address -->
                <div class="mt-4">
                    <x-input-label for="email" :value="__('Email')" />
                    <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" value="{{$customer->email}}" required autocomplete="username" />
                    <x-input-error :messages="$errors->get('email')" class="mt-2" />
                </div>

                <!-- User Name -->
                <div>
                    <x-input-label for="user_name" :value="__('User Name')" />
                    <x-text-input id="user_name" class="block mt-1 w-full" type="text" name="user_name" value="{{$customer->user_name}}" required autofocus autocomplete="last_name" />
                    <x-input-error :messages="$errors->get('user_name')" class="mt-2" />
                </div>

                <!-- Salary -->
                <div>
                    <x-input-label for="salary" :value="__('Salary')" />
                    <x-text-input id="salary" class="block mt-1 w-full" type="number" name="salary" value="{{$customer->salary}}" required autofocus autocomplete="salary" />
                    <x-input-error :messages="$errors->get('salary')" class="mt-2" />
                </div>
                <!-- Status -->
                <div>
                    <x-input-label for="status" :value="__('Status')" />
                    <x-text-input id="status" class="block mt-1" type="checkbox" name="status" required autofocus autocomplete="status" />
                    <x-input-error :messages="$errors->get('status')" class="mt-2" />
                </div>

                <div class="flex items-center justify-end mt-4">
                    <x-primary-button class="ml-4">
                        {{ __('Save') }}
                    </x-primary-button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
