<x-app-layout>
    @if($errors->any())
        {{ implode('', $errors->all(':message')) }}
    @endif
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
                <!-- First Name -->
                <div>
                    <x-input-label for="name" :value="__('First Name')" />
                    <x-text-input id="name" class="block mt-1 w-full" type="text" name="first_name" value="{{$customer->first_name}}" disabled />
                </div>
                <!-- Last Name -->
                <div>
                    <x-input-label for="name" :value="__('Last Name')" />
                    <x-text-input id="name" class="block mt-1 w-full" type="text" name="last_name"  value="{{$customer->last_name}}" disabled />
                </div>

                <!-- Email Address -->
                <div class="mt-4">
                    <x-input-label for="email" :value="__('Email')" />
                    <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" value="{{$customer->email}}" disabled />
                    <x-input-error :messages="$errors->get('email')" class="mt-2" />
                </div>

                <!-- User Name -->
                <div>
                    <x-input-label for="user_name" :value="__('User Name')" />
                    <x-text-input id="user_name" class="block mt-1 w-full" type="text" name="user_name" value="{{$customer->user_name}}" disabled />
                </div>

                <!-- Salary -->
                <div>
                    <x-input-label for="salary" :value="__('Salary')" />
                    <x-text-input id="salary" class="block mt-1 w-full" type="number" name="salary" value="{{$customer->salary}}" disabled />
                </div>
                <!-- Status -->
                <div>
                    <x-input-label for="status" :value="__('Status')" />
                    <x-text-input id="status" class="block mt-1" type="checkbox" name="status" @checked($customer->status) disabled />
                </div>

        </div>
    </div>
</x-app-layout>
