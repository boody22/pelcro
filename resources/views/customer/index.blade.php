<x-app-layout>
    @if($errors->any())
        {{ implode('', $errors->all(':message')) }}
    @endif
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create New Customer') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-12 space-y-6">
            <div >
                <table class="table table-striped ">
                    <thead>
                    <tr>
                        <th class="bg-gray-100">ID</th>
                        <th class="bg-gray-100">First Name</th>
                        <th class="bg-gray-100">Last Name</th>
                        <th class="bg-gray-100">Username</th>
                        <th>Email</th>
                        <th colspan="3">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($customers as $key => $value)
                        <tr>
                            <td>{{ $value->id }}</td>
                            <td>{{ $value->first_name }}</td>
                            <td>{{ $value->last_name }}</td>
                            <td>{{ $value->user_name }}</td>
                            <td>
                                <a href="mailto:{{ $value->email }}" title="Send email to {{ $value->email }}">
                                    {{ $value->email }}
                                </a>
                            </td>
                            <td>
                                {!! Form::open(array('url' => 'customers/' . $value->id, 'class' => 'pull-right')) !!}
                                {!! Form::hidden('_method', 'DELETE') !!}
                                {!! Form::button('<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i> Delete', array('class' => 'btn btn-danger btn-block btn-flat','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete User', 'data-message' => 'Are you sure you want to delete this user ?')) !!}
                                {!! Form::close() !!}
                            </td>
                            <td>
                                <a class="btn btn-small btn-success btn-block btn-flat"
                                   href="{{ URL::to('customers/' . $value->id) }}">Show</a>
                            </td>
                            <td>
                                <a class="btn btn-small btn-info btn-block btn-flat"
                                   href="{{ URL::to('customers/' . $value->id . '/edit') }}">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>


        </div>
    </div>
</x-app-layout>
