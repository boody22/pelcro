<?php

namespace App\Listeners;

use App\Events\CustomerCreated;
use App\Mail\WelcomeCustomerEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendWelcomeCustomerMail implements ShouldQueue
{

    /**
     * Handle the event.
     *
     * @param CustomerCreated $event
     * @return void
     */
    public function handle(CustomerCreated $event): void
    {
        \Mail::to($event->customer->email)->send(
            new WelcomeCustomerEmail($event->customer)
        );
    }
}
