<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CreateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'first_name' => ['required', 'string', 'max:100'],
            'last_name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'email', 'max:100'],
            'user_name' => ['required', 'max:100'],
            'salary' => ['required', 'numeric'],
            'status' => ['required'],
        ];
    }

    public function prepareForValidation()
    {
        $data = $this->all();
        $checked = 0;
        if (isset($data['status']) and $data['status'] == 'on')
            $checked = 1;
        $data['status'] = $checked;
        $this->replace($data);
    }
}
