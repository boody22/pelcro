<?php

namespace App\Http\Controllers\API;

use App\Events\CustomerCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\CreateCustomerRequest;
use App\Http\Requests\API\UpdateCustomerRequest;
use App\Http\Resources\API\CustomerResource;
use App\Models\Customer;
use Illuminate\Database\Eloquent\Model;

class CustomerController extends Controller
{
    private Model $model;
    public function __construct(Customer $model)
    {
        $this->model = $model;
    }
    public function index(): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'status' => true,
            'data' => CustomerResource::collection($this->model->all())
        ]);
    }

    public function store(CreateCustomerRequest $request): \Illuminate\Http\JsonResponse
    {
        $customer = $this->model->create($request->validated());
        event(new CustomerCreated($customer));

        return response()->json([
            'status' => true,
            'message' => "customer-created",
            'data' => new CustomerResource($customer)
        ]);
    }

    public function show($id): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'status' => true,
            'message' => "customer-created",
            'data' => new CustomerResource($this->model->find($id))
        ]);
    }

    public function update(UpdateCustomerRequest $request, $id): \Illuminate\Http\JsonResponse
    {
        Customer::where('id', $id)->update($request->validated());
        return response()->json([
            'status' => true,
            'message' => "customer-update"
        ]);
    }

    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        $customer = Customer::find($id);
        $customer->delete($id);

        return response()->json([
            'status' => true,
            'message' => "customer-deleted"
        ]);
    }
}
