<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\UserLoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(UserLoginRequest $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user =  User::where('email', $request->email)->first();
            return response()->json([
                'status' => true,
                'token' => $user->createToken('api-token')->plainTextToken
            ]);
        }
        return response()->json([
            'status' => false,
            'token' => "invalid credentials"
        ], 401);
    }
}
