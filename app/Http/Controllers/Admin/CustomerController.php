<?php

namespace App\Http\Controllers\Admin;

use App\Events\CustomerCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateCustomerRequest;
use App\Http\Requests\Admin\UpdateCustomerRequest;
use App\Models\Customer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Illuminate\Support\Facades\Redirect;

class CustomerController extends Controller
{

    private Model $model;
    public function __construct(Customer $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('customer.index', [
            'customers' => $this->model->all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateCustomerRequest $request
     * @return RedirectResponse
     */
    public function store(CreateCustomerRequest $request): RedirectResponse
    {
        $customer = $this->model->create($request->validated());
        event(new CustomerCreated($customer));

        return Redirect::route('customers.index')->with('status', 'customer-created');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function show(int $id): View
    {
        return view('customer.show', [
            'customer' => $this->model->find($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        return view('customer.edit', [
            'customer' => Customer::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCustomerRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateCustomerRequest $request, int $id): RedirectResponse
    {
        Customer::where('id', $id)->update($request->validated());
        return Redirect::route('customers.index')->with('status', 'customer-updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id): Response
    {
        $customer = Customer::find($id);
        $customer->delete($id);

        return Redirect::route('customers.index')->with('status', 'customer-deleted');
    }
}
